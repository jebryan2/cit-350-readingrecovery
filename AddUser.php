<?php
	$page='adduser';
 	session_start();
	require 'Header.php';
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="CSS/Website.css">
		<link rel="stylesheet" type="text/css" media=
		"only screen and (max-width: 480px), only screen and (max-device-width: 480px)"
		href="CSS/Mobile.css">
</head>
<body>
	<br>
	<br>
	<br>
	<div class="getaccount">
		<h2 class="aligncenter">Add User</h2>
		<form method="post" action="#">
			<p>
                   <label>First Name:</label>
			        <input type= "text" placeholder="teacher firstname" name="fname" required> 
               </p><p>
                   <label>Last Name:</label>
			        <input type= "text" placeholder="teacher lastname" name="lname" required> 
               </p>		 
              
               <p>
                   <label>School:</label>
			        <input type= "text" placeholder="teacher school" name="school" required> 
               </p>

 			<p>
                   <label>Email:</label>
			        <input type= "email" placeholder="teacheremail@somewhere.edu" name="email" required> 
               </p>

			 <p>
                   <label>Default Password:</label>
			        <input type= "password" placeholder="Default password" name="tpwd" required> 
               </p>
               <button type="Submit" value="Submit">Add User</button> 
           </form>
	    </div>
</body>
</html>