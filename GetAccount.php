<?php
    $page='getaccount';    
    session_start();
	include 'Header.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="CSS/Website.css">
        <link rel="stylesheet" type="text/css" 
            media="only screen and (max-width: 480px), only screen and (max-device-width: 480px)" href="CSS/Mobile.css">
</head>
<body>
	<br>
	<br>
	<div class="getaccount">
		<h2 class="aligncenter">Request Account</h2>
		<form method="post" action="#">
			<p>
                   <label>First Name:</label>
			        <input type= "text" placeholder="your firstname" name="fname" required> 
               </p><p>
                   <label>Last Name:</label>
			        <input type= "text" placeholder="your lastname" name="lname" required> 
               </p>		 
               <p>
                   <label>Email:</label>
			        <input type= "email" placeholder="youremail@somewhere.edu" name="email" required> 
               </p>

               <p>
                   <label>School:</label>
			        <input type= "text" placeholder="your school" name="school" required> 
               </p>
               <button type="Submit" value="Submit">Request Account</button> 
           </form>
	    </div>
</body>
</html>