<?php
    $page='login';    
    session_start();
	include 'Header.php';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Login Portal</title>
        <link rel="stylesheet" type="text/css" href="CSS/Website.css">
        <link rel="stylesheet" type="text/css" 
            media="only screen and (max-width: 480px), only screen and (max-device-width: 480px)" href="CSS/Mobile.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
        </script>
    </head>
    <style type="text/css">
	</style>
    <body>
	    <br>
	    <br>
	    <div class="login">
		<h2 class="aligncenter"> Login</h2>
	    <?php
                if (isset($_SESSION['error']['badpassword']))
               {
                   echo $_SESSION['error']['badpassword']; 
                   unset( $_SESSION['error']['badpassword']); 
               }

                if (isset($_SESSION['error']['notloggedin']))
               {
                   echo $_SESSION['error']['notloggedin'];
                    unset( $_SESSION['error']['notloggedin']);
               }
                if (isset($_SESSION['success']['created']))
               {
                   echo $_SESSION['success']['created'];
                    unset( $_SESSION['success']['created']);
               }

           ?>
		<form method="post" action="CheckLogin.php">
					 
               <p>
                   <label>Email:</label>
			        <input type= "email" placeholder="youremail@somewhere.edu" name="email" required> 
               </p>

               <p>
                   <label>Password:</label>
			        <input type= "password" placeholder="yourpassword" name="password" required> 
               </p>
               <button type="Submit" value="Submit">Login</button> 
               <p>
               	<em>Need an account? 
               	<a href="GetAccount.php">Click here to request one.</em></a>
               </p>
           </form>
	    </div>
	 </body>
 </html>