<?php
session_start();
$_SESSION['n-count']=2;
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Reading Recovery <?php echo ucfirst($page);?></title>
	<!--<link rel="icon" href="favicon.png" sizes="16x16 32x32 48x48 64x64"> width="354px" height="176px" -->
	 <link href='http://fonts.googleapis.com/css?family=The+Girl+Next+Door' rel='stylesheet' type='text/css'>
	 <link href='http://fonts.googleapis.com/css?family=Homemade+Apple' rel='stylesheet' type='text/css'>
</head>
<body>
	<!-- Logo -->
	 <div  class="floatright">  
		<a href="Index.php"><img src="Images/ReadingRecoveryFlip.png" width="221px" height="142px"  
		class="floatright" id="logo" alt="Reading Recovery Logo"/></a> <!--553 X 356 Original size-->
	</div>       	
	<div class="menu">
		<!-- If user has logged in-->
		<?php
			if (($_SESSION['usertype']=='registered') and ($page!=='logout'))
			{
				//echo '<em class="greeting">Welcome, '. $_SESSION['name'].'!</em>';
		?> 
				 <button class="floatleft aligncenter" id="logout">
				 	<a href="Logout.php"> Logout</a>
				 </button>

		<!-- If page is View-->
		<?php
			if ($page=='view') 
			{ 
		?>
			   <button class="selected floatleft aligncenter">
			   <a href="ViewProgress.php">Student Progress</a>
			   </button>

		<?php
			}// end if view
			
			else 
			{
		?>
				<button class="floatleft aligncenter">
				<a href="ViewProgress.php">Student Progress</a>
				</button>
		
		<!-- If page is Notification-->
		<?php
			}//end not view
		
			if ($page=='notification') 
			{
		?>
				<button class="selected floatleft aligncenter">
					<a href="Notification.php">Notifications<?php echo $_SESSION['note-count']?></a> 
			     </button>
		<?php
			}//end if note
		
			else 
			{
		?>
				 <button class="floatleft aligncenter">
				 	<a href="Notification.php">Notifications<?php echo $_SESSION['note-count']?></a>
				 </button>
		
		<!-- if page is Input-->
		<?php
			}//end not note

			if ($page=='input') 
			{
		?>
				 <button class="selected floatleft aligncenter">
				 	<a href="InputScores.php">Input Progress</a> 
				 </button>
		<?php
			}//end if input
			
			else 
			{
		?>
				<button class="floatleft aligncenter">
					<a href="InputScores.php">Input Progress</a>
				</button>

		<!-- if page is AddUser-->
		<?php
			}//end not input

			if ($page=='adduser') 
			{
		?>
				<button class="selected floatleft aligncenter">
					<a href="AddUser.php"> Add User</a> 
				</button>
	    <?php
			}//end if adduser
		
			else 
			{
		?>
				 <button class="floatleft aligncenter">
				 	<a href="AddUser.php">Add User</a>
				 </button>

		<!-- if page is AddStudent-->
		<?php
			}//end not adduser

			if ($page=='addstudent') 
			{
		?>
				 <button class="selected floatleft aligncenter">
			      	<a href="AddStudent.php"> Add Student</a> 
				 </button>

	    	<?php
			}//end if adduser
		
			else 
			{
		?>
				 <button class="floatleft aligncenter">
				 	<a href="AddStudent.php">Add Student</a>
				 </button>

		<?php 
			}//end not adduser
	
			}//end if logged in
		
			else if (($_SESSION['usertype']!=='registered') or ($page=='logout'))
		 	{
	     ?>
				<button class="floatleft aligncenter">
				 	<a href="Login.php"> Login</a>
				</button>
		
				<button class="floatleft aligncenter">
					<a href="GetAccount.php">Request Account</a>
				</button>
		<?php
			}// end if not logged in
		?>
	</div>
	<!--<h2 class="aligncenter page"> <?php echo ucfirst($page);?></h2>-->
</body>
</html>